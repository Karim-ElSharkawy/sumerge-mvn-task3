package com.sumerge.exception;

public class DataNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -124151512L;

    public DataNotFoundException(String message){
        super(message);
    }
}
