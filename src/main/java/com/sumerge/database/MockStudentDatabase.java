package com.sumerge.database;

import com.sumerge.model.Student;

import java.util.ArrayList;

public class MockStudentDatabase {
    private static ArrayList<Student> listStudents = new ArrayList<Student>();
    static{
        // Temporary initialization instead of DB
        ArrayList<Student> initList = new ArrayList<>();
        initList.add(new Student("Karim", "2016", "CS"));
        initList.add(new Student("Karim2", "2017", "CS"));
        initList.add(new Student("Karim3", "2018", "CS"));
        initList.add(new Student("Karim4", "2017", "CS"));
        initList.add(new Student("Karim", "2016", "CS"));
        initList.add(new Student("Karim2", "2017", "CS"));
        initList.add(new Student("Karim13", "2018", "CS"));
        initList.add(new Student("Karim14", "2017", "CS"));
        initList.add(new Student("Karim1", "2016", "CS"));
        initList.add(new Student("Karim12", "2017", "CS"));
        initList.add(new Student("Karim13", "2018", "CS"));
        initList.add(new Student("Karim14", "2017", "CS"));

        setListStudents(initList);
    }
    public static void setListStudents(ArrayList<Student> newList){
        listStudents = newList;
    }
    public static ArrayList<Student> getListStudents() {
        return listStudents;
    }

    public static Student getStudentByID(int id){
        return listStudents.get(id);
    }
    public static void addStudent(Student stud){
        listStudents.add(stud);
    }
    public static Student deleteStudent(int id){
        Student temp = listStudents.remove(id);
        return temp;
    }
}
