package com.sumerge.database;

import com.sumerge.model.Class;

import java.util.ArrayList;

public class MockClassDatabase {
    private static ArrayList<Class> listClasses = new ArrayList<>();

    static {
        // Temporary initialization instead of DB
        ArrayList<Integer> temp = new ArrayList<>();
        temp.add(0);
        temp.add(1);
        temp.add(3);
        temp.add(5);
        temp.add(9);
        temp.add(10);
        listClasses.add(new Class("22ICS09H", "Backend", temp));
        temp = new ArrayList<>();
        temp.add(2);
        temp.add(4);
        temp.add(6);
        temp.add(7);
        temp.add(8);
        temp.add(11);
        listClasses.add(new Class("22ICS04C", "Frontend", temp));
    }

    public static ArrayList<Class> getListClasses() {
        return listClasses;
    }

    public static Class getClassByCode(String code) {
        for (Class cls : listClasses){
            if(cls.getCode().equals(code)){
                return cls;
            }
        }
        return null;
    }

    public static void addClass(Class cls) {
        listClasses.add(cls);
    }

    public static Class deleteClass(String code) {
        for (Class cls : listClasses){
            if(cls.getCode().equals(code)){
                listClasses.remove(cls);
                return cls;
            }
        }
        return null;
    }

    // Operations on Students in Classes
    public static void addStudentsToClass(String code, ArrayList<Integer> students){
        for (Class cls : listClasses){
            if(cls.getCode().equals(code)){
                cls.getStudentIds().addAll(students);
                return;
            }
        }
    }
    public static void removeStudentFromClassById(String code, Integer id){
        for (Class cls : listClasses){
            if(cls.getCode().equals(code)){
                cls.getStudentIds().remove(id);
            }
        }
    }
}
