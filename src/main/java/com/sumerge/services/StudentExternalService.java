package com.sumerge.services;

import com.sumerge.model.Student;

public interface StudentExternalService {
    public Student studentLookUp(int id);
}
