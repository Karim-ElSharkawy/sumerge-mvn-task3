package com.sumerge.services;

import com.sumerge.model.Class;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface ClassService {
    // -- GET OPERATIONS --
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getClasses(@QueryParam("code") String code,
                               @QueryParam("start") int start,
                               @QueryParam("size") int size);
    @Path("/class/{code}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getClass(@PathParam("code") String code);

    @Path("/class/{code}/students")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getClassStudents(@PathParam("code") String code);

    // -- GET OPERATIONS ENDS --

    // -- POST OPERATIONS --
    @POST
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addClass(Class cls);

    // -- POST OPERATIONS ENDS --

    // -- PUT OPERATIONS --
    @Path("/class/{code}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateClass(@PathParam("code") String code, Class cls);

    // -- PUT OPERATIONS ENDS --

    // -- DELETE OPERATIONS --
    @Path("/class/{code}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteStudent(@PathParam("code") String code);

    // -- DELETE OPERATIONS ENDS --
}
