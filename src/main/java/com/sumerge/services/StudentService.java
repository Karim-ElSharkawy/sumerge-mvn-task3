package com.sumerge.services;

import com.sumerge.model.Student;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface StudentService {
    // -- GET OPERATIONS --
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStudents(@QueryParam("year") String year,
                                @QueryParam("start") int start,
                                @QueryParam("size") int size);

    @Path("/student/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStudent(@PathParam("id") Integer id);

    // -- GET OPERATIONS ENDS --
    // -- POST OPERATIONS --
    @POST
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addStudent(Student stud);
    // -- POST OPERATIONS ENDS --

    // -- PUT OPERATIONS --
    @Path("/student/{id}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateStudent(@PathParam("id") Integer id, Student tempStud);
    // -- PUT OPERATIONS ENDS --

    // -- DELETE OPERATIONS --
    @Path("/student/{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteStudent(@PathParam("id") Integer id);
    // -- DELETE OPERATIONS ENDS --
}
