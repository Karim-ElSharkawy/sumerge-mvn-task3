package com.sumerge.model;

import java.util.ArrayList;

public class Class {
    String code;
    String name;
    ArrayList<Integer> studentIds;

    public Class(String code, String name, ArrayList<Integer> studentIds) {
        this.code = code;
        this.name = name;
        this.studentIds = studentIds;
    }

    public boolean isEnrolled(int id){
        for (int studentId : studentIds){
            System.out.println("studentID: " + studentId);
            if(studentId == id) {
                return true;
            }
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public ArrayList<Integer> getStudentIds() {
        return studentIds;
    }

    public void setStudentIds(ArrayList<Integer> studentIds) {
        this.studentIds = studentIds;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Class{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", studentIds=" + studentIds +
                '}';
    }
}
