package com.sumerge.resources;

import com.sumerge.database.StudentDatabase;
import com.sumerge.exception.DataNotFoundException;
import com.sumerge.model.Student;
import com.sumerge.services.StudentService;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.stream.Collectors;

@Path("/students")
public class StudentResource implements StudentService {

    // -- GET OPERATIONS --
    public Response getStudents(@QueryParam("year") String year,
                                @QueryParam("start") int start,
                                @QueryParam("size") int size) {
        ArrayList<Student> tempList = StudentDatabase.getListStudents();
        // Query Year
        if (year != null && !year.equals("")) {
            tempList = (ArrayList<Student>) StudentDatabase.getListStudents().stream().filter((Student) -> Student.getEnrollmentYear().equals(year)
            ).collect(Collectors.toList());

            if (tempList.size() == 0) {
                return Response.status(Response.Status.NOT_FOUND).entity("year not found").build();
            }
            return Response.ok(
                    tempList
                    , MediaType.APPLICATION_JSON).build();
        }
        // Query Pagination
        else if (start >= 0 && size > 0 ){
            if (start - 1 >= StudentDatabase.getListStudents().size())
                return Response.status(Response.Status.NOT_FOUND).entity("ID out of bounds").build();

            tempList = new ArrayList<>();
            for (int i = (start - 1); i < (start - 1 + size); i++) {
                if (i == StudentDatabase.getListStudents().size()) {
                    break;
                }
                tempList.add(StudentDatabase.getListStudents().get(i));
            }
        }
        // Return Full List
        return Response.ok(
                tempList, MediaType.APPLICATION_JSON).build();
    }

    public Response getStudent(@PathParam("id") Integer id) {
        if (id >= StudentDatabase.getListStudents().size() || id < 0 )
            throw new DataNotFoundException("Id not Found");

        return Response.ok(
                StudentDatabase.getStudentByID(id), MediaType.APPLICATION_JSON).build();
    }

    // -- GET OPERATIONS ENDS --
    // -- POST OPERATIONS --
    public Response addStudent(Student stud) {
        StudentDatabase.addStudent(stud);
        return Response.status(Response.Status.CREATED).entity("Student Added!").build();
    }
    // -- POST OPERATIONS ENDS --

    // -- PUT OPERATIONS --
    public Response updateStudent(@PathParam("id") Integer id, Student tempStud) {
        if (id >= StudentDatabase.getListStudents().size() || id < 0)
            throw new DataNotFoundException("Id not Found");

        String msg = "";
        if (tempStud.getName() != null) {
            StudentDatabase.getListStudents().get(id).setName(tempStud.getName());
            msg += "Name, ";
        }
        if (tempStud.getEnrollmentYear() != null) {
            StudentDatabase.getListStudents().get(id).setEnrollmentYear(tempStud.getEnrollmentYear());
            msg += "Enrollment Year, ";
        }
        if (tempStud.getMajor() != null) {
            StudentDatabase.getListStudents().get(id).setMajor(tempStud.getMajor());
            msg += "Major, ";
        }

        if (msg != "") {
            return Response.status(Response.Status.OK).entity("(" + msg.substring(0, msg.length() - 2) + ") was updated").build();
        } else
            return Response.status(Response.Status.NOT_FOUND).entity("Field not found").build();

    }
    // -- PUT OPERATIONS ENDS --

    // -- DELETE OPERATIONS --
    public Response deleteStudent(@PathParam("id") Integer id) {
        return Response.status(Response.Status.OK).entity(StudentDatabase.deleteStudent(id)).build();
    }
    // -- DELETE OPERATIONS ENDS --
}