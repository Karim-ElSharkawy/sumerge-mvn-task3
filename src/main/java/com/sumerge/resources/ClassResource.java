package com.sumerge.resources;

import com.sumerge.database.ClassDatabase;
import com.sumerge.database.StudentDatabase;
import com.sumerge.exception.DataNotFoundException;
import com.sumerge.model.Class;
import com.sumerge.model.Student;
import com.sumerge.services.ClassService;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("/classes")
public class ClassResource implements ClassService {

    // -- GET OPERATIONS --
    public Response getClasses(@QueryParam("code") String code,
                               @QueryParam("start") int start,
                               @QueryParam("size") int size) {
        ArrayList<Class> tempList = ClassDatabase.getListClasses();

        // Query Pagination
        if (start >= 0 && size > 0) {
            if (start - 1 >= ClassDatabase.getListClasses().size())
                return Response.status(Response.Status.NOT_FOUND).entity("ID out of bounds").build();

            tempList.clear();
            for (int i = (start - 1); i < (start - 1 + size); i++) {
                if (i == ClassDatabase.getListClasses().size()) {
                    break;
                }
                tempList.add(ClassDatabase.getListClasses().get(i));
            }
        }

        // Return Full List
        return Response.ok(
                tempList, MediaType.APPLICATION_JSON).build();
    }

    public Response getClass(@PathParam("code") String code) {

        // If Query Code is present, check code and return response.
        Class classByCode = ClassDatabase.getClassByCode(code);
        if (classByCode == null) {
            throw new DataNotFoundException("Class Code not found");
        }

        return Response.ok(classByCode, MediaType.APPLICATION_JSON).build();

    }

    public Response getClassStudents(@PathParam("code") String code) {

        // If Query Code is present, check code and return response.
        Class classByCode = ClassDatabase.getClassByCode(code);
        if (classByCode == null) {
            throw new DataNotFoundException("Class Code not found");
        }
        ArrayList<Student> studentList = new ArrayList<>();
        for (int studentID : classByCode.getStudentIds()) {
            Student std = StudentDatabase.getStudentByID(studentID);
            if (std == null) {
                continue;
            }

            studentList.add(std);
        }
        if (studentList.size() == 0) {
            throw new DataNotFoundException("Student IDs not found");
        }

        return Response.ok(studentList, MediaType.APPLICATION_JSON).build();

    }
    // -- GET OPERATIONS ENDS --

    // -- POST OPERATIONS --
    public Response addClass(Class cls) {
        ClassDatabase.addClass(cls);
        return Response.status(Response.Status.CREATED).entity("Class Added!").build();
    }
    // -- POST OPERATIONS ENDS --

    // -- PUT OPERATIONS --
    public Response updateClass(@PathParam("code") String code, Class cls) {
        Class classByCode = ClassDatabase.getClassByCode(code);
        if (classByCode == null || code == null) {
            throw new DataNotFoundException("Class Code not found");
        }

        String msg = "";
        if (cls.getName() != null) {
            classByCode.setName(cls.getName());
            msg += "Name, ";
        }
        if (cls.getStudentIds() != null) {
            for (int studentID : cls.getStudentIds()) {
                System.out.println(studentID);
                if (classByCode.isEnrolled(studentID)) {
                    continue;
                }
                classByCode.getStudentIds().add(studentID);
            }
            msg += "Student IDs, ";
        }

        if (!msg.equals("")) {
            return Response.status(Response.Status.OK).entity("(" + msg.substring(0, msg.length() - 2) + ") was updated").build();
        } else
            return Response.status(Response.Status.NOT_FOUND).entity("Field not found").build();

    }
    // -- PUT OPERATIONS ENDS --

    // -- DELETE OPERATIONS --
    public Response deleteStudent(@PathParam("code") String code) {
        Class deletedClass = ClassDatabase.deleteClass(code);
        if (deletedClass == null) {
            throw new DataNotFoundException("Class Code not Found");
        }
        return Response.status(Response.Status.OK).entity("Deleted Class with code " + code).build();
    }
    // -- DELETE OPERATIONS ENDS --
}
