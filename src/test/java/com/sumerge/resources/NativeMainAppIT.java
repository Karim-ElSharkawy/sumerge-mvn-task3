package com.sumerge.resources;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeMainAppIT extends studentResourceTest {

    // Execute the same tests but in native mode.
}