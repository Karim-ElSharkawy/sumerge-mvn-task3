package com.sumerge.resources;

import com.sumerge.database.MockStudentDatabase;
import com.sumerge.model.Student;
import com.sumerge.services.StudentExternalService;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.*;
import org.mockito.Spy;

import static org.mockito.Mockito.*;

@QuarkusTest
public class StudentControllerTests {
    @Spy
    StudentController studentController;
    StudentExternalService studentDatabaseServer;
    StudentExternalService studentWebServer;

    @BeforeEach
    void init() {
        studentDatabaseServer = mock(StudentExternalService.class);
        studentWebServer = mock(StudentExternalService.class);

        studentController = spy(new StudentController());
        studentController.setDatabaseService(studentDatabaseServer);
        studentController.setWebService(studentWebServer);
    }

    @AfterEach
    void cleanup() {
        studentController = null;
        studentDatabaseServer = null;
        studentWebServer = null;
    }

    @Test
    @DisplayName("Add Student Successfully")
    /* Create and add a new student,
     then extract latest student from DB,
     then compare with overridden equals() function */
    public void addStudent_AddedSuccessfully_IfCorrectInfo() {
        Student newStudent = new Student("New Stud", "2021", "SE");

        System.out.println("BeforeSpy: " + studentController.toString());
        doReturn("AfterSpy: 123Spy Method Overridden Successfully123").when(studentController).toString();
        System.out.println(studentController.toString());

        studentController.addStudent(newStudent);

        int index = MockStudentDatabase.getListStudents().size() - 1;
        Student latestStudentAdded = (Student) studentController.getStudent(index).getEntity();

        Assertions.assertEquals(newStudent, latestStudentAdded);
    }

    @Test
    @DisplayName("Found Student in Database")
    /* Find Student in Database and ignore WebService since Student is found. */
    public void findStudentById_FoundSuccessfullyInDatabase() {
        when(studentDatabaseServer.studentLookUp(anyInt())).thenReturn(new Student("Karim Abdel-Wahab", "2016", "CS"));

        studentController.getStudent(4);

        verify(studentDatabaseServer, times(1)).studentLookUp(4);
        verify(studentWebServer, never()).studentLookUp(4);
    }

    @Test
    @DisplayName("Found Student in WebService instead of Database")
    /* Look student in Database and Database fails, then look in WebService. */
    public void findStudentById_FoundSuccessfullyInWebService_DataBaseFailed() {
        when(studentDatabaseServer.studentLookUp(anyInt())).thenReturn(null);
        when(studentWebServer.studentLookUp(anyInt())).thenReturn(new Student("Karim Abdel-Wahab", "2016", "CS"));

        studentController.getStudent(4);

        verify(studentDatabaseServer).studentLookUp(4);
        verify(studentWebServer).studentLookUp(4);
    }
}